package main

import (
	"gitlab.com/clem77/hashlife/algo"
	"time"
	"math"
	"fmt"
)

func main() {
	universeLevel := 5
	nodeOrigin, table := algo.Initialize(uint16(universeLevel))
	nodeOrigin.Display()
	print("\n")
	time.Sleep(100 * time.Millisecond)
	start:=time.Now()
	time1,_:= time.ParseDuration("30ms")
	iteration := float64(0)

	previous := nodeOrigin	
	for i := 0; i < 1000; i++ {
		thor := algo.Rebuild(previous, table)
		thor.Next(table)
		previous = thor.Result
		iteration = iteration + math.Pow(2, float64(universeLevel-1))
		if i%10 == 0 || i<10 {
			elapsed := time.Since(start)
			fmt.Printf("\n %d iteration: %d - elapsed = %s -- " +
				"table length: %d", i, int(iteration), elapsed, table.Longueur())
			if elapsed < time1 && i>1 {
				previous, table = algo.ExtendTree(thor.Result, table)
				fmt.Printf(" --> Tree extended! new level = ")
				fmt.Print(previous.GetLevel())
			}
			start = time.Now()
		}
	}
	time.Sleep(100 * time.Millisecond)
	fmt.Println("\n")
	previous.Display()
	fmt.Println("\n")
	fmt.Print(table.Longueur())
}