package algo

import (
	"fmt"
	"encoding/hex"
	"errors"
	"sync"
)

type Table struct {
	list map[string]*Node
	lock sync.RWMutex
}

func (t *Table) Longueur() int {
	return len(t.list)
}


func (t *Table) Display() {
	t.lock.RLock()
	defer t.lock.RUnlock()
	for _, n := range t.list {
		n.Display()
		fmt.Printf(" (%v)", hex.EncodeToString(n.sign[:]))
		if n.Result != nil {
			n.Result.Display()
		} else {
			fmt.Print(" nil")
		}
		fmt.Println("")
	}
}

// true if not in the table
// false if signature already exist in the list
func (t *Table) SaveNode(n *Node) bool {
	t.lock.Lock()
	defer t.lock.Unlock()
	signature := string(n.sign[:])
	_, ok := t.list[signature]
	if !ok {
		t.list[signature] = n
		return true
	}
	return false
}

// true if the signature of the node is in the list
func (t *Table) Contain(n *Node) bool {
	t.lock.RLock()
	defer t.lock.RUnlock()
	signature := string(n.sign[:])
	_, ok := t.list[signature]
	if !ok {
		return false
	}
	return true
}

func (t *Table) GetNode(sign [32]byte) *Node {
	t.lock.RLock()
	defer t.lock.RUnlock()
	signature := string(sign[:])
	savedNode, ok := t.list[signature]
	if !ok {
		panic("arg")
		return nil
	}
	return savedNode
}


func (t *Table) SetResult(n, m *Node) error {
	t.lock.Lock()
	defer t.lock.Unlock()
	signature := string(n.sign[:])
	node, ok := t.list[signature]
	if !ok {
		return errors.New("")
	}
	node.Result = m
	return nil
}
