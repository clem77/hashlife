package algo

import "fmt"

func NextBand(band []int8) []int8 {
	next := make([]int8, len(band))
	for i := 1; i < len(band)-1; i++ {
		if band[i-1] == band[i+1] {
			next[i] = 1-band[i] 
		} else {
			next[i] = band[i]
		}
	}
	if band[1] == band[len(band)-1] {
		next[0] = 1-band[0]
	} else {
		next[0] = band[0]
	}
	if band[0] == band[len(band)-2] {
		next[len(band)-1] = 1-band[len(band)-1]
	}  else {
		next[len(band)-1] = band[len(band)-1]
	}
	return next
} 

func (n *Node) BuildBand() []int8{
	if n.level == 0 {
		return []int8{n.value}
	}
	return append(n.Left.BuildBand(), n.Right.BuildBand()...)
}

func DisplayBand(band []int8){
	for i:= 0; i<len(band); i++ {
		fmt.Print(band[i])
	}
}