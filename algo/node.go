package algo

import (
	"crypto/sha256"
	//"math/rand"
	"encoding/binary"
	"fmt"
)

type Node struct {
	level  uint16
	sign   [32]byte
	value  int8
	Left   *Node
	Right  *Node
	Result *Node
}

func (n *Node) GetLevel() uint16 {
	return n.level
}

func (n *Node) Display() {
	if n.level == 0 {
		fmt.Print(n.value)
		return
	}
	n.Left.Display()
	n.Right.Display()
}

func (n *Node) DisplayTree() {
	if n.level == 0 {
		//fmt.Println(n)
		return
	}
	fmt.Println(n)
	n.Left.DisplayTree()
	n.Right.DisplayTree()
}

func (n *Node) Next(table *Table) {
	//defer TimeTaken(time.Now(), "Next")
	if n.Result != nil {
		return
	}
	if n.level == 2 {
		n.classicCalc(table)
		return
	}

	n.Left.Next(table) // --> rouge gauche
	n.Right.Next(table) // --> rouge droite

	tmpNode := &Node{
		level: n.level-1,
		Left:  n.Left.Right,
		Right: n.Right.Left,
	}
	tmpNode.initSignature()
	if table.Contain(tmpNode) {
		tmpNode = table.GetNode(tmpNode.sign)
	}
	tmpNode.Next(table) // --> rouge centre

	leftBlue := &Node{
		level: n.level-1,
		Left: n.Left.Result,
		Right: tmpNode.Result,
	}
	leftBlue.initSignature()
	// fmt.Println("\ntic : ")
	// leftBlue.Display()


	rightBlue := &Node{
		level: n.level-1,
		Left: tmpNode.Result,
		Right: n.Right.Result,
	}
	rightBlue.initSignature()
	// rightBlue.Display()
	

	if table.Contain(leftBlue) {
		leftBlue = table.GetNode(leftBlue.sign)
	}
	if table.Contain(rightBlue) {
		rightBlue = table.GetNode(rightBlue.sign)
	}

	leftBlue.Next(table)
	rightBlue.Next(table)

	result := &Node{
		level: n.level-1,
		Left: leftBlue.Result,
		Right: rightBlue.Result,
	}
	result.initSignature()

	// fmt.Println("\nresult from inside : ")
	// result.Display()

	if table.Contain(result.Left) {
		result.Left = table.GetNode(result.Left.sign)
	}
	if table.Contain(result.Right) {
		result.Right = table.GetNode(result.Right.sign)
	}

	if table.Contain(result) {
		finalResult := table.GetNode(result.sign)
		n.Result = finalResult
	} else {
		table.SaveNode(result)
		n.Result = result
	}
}

// for node of level 2
func (n *Node) classicCalc(table *Table){
	zeroNodeLeft := &Node{level:0}
	zeroNodeRight := &Node{level:0}

	// if n.Left.Left.sign == n.Right.Left.sign {
	// 	zeroNodeLeft.value = 1 - n.Left.Right.value
	// } else {
	// 	zeroNodeLeft.value  = n.Left.Right.value
	// }
	// if n.Left.Right.sign == n.Right.Right.sign {
	// 	zeroNodeRight.value = 1 - n.Right.Left.value
	// } else {
	// 	zeroNodeRight.value = n.Right.Left.value
	// }

	//zeroNodeLeft.value = int8(0)
	//zeroNodeRight.value = int8(0)


	//rule 30
	//for result.left:
	if n.Left.Left.value == 1 && n.Left.Right.value ==0 && n.Right.Left.value == 0 {
		zeroNodeLeft.value = 1
	} else if n.Left.Left.value == 0 && n.Left.Right.value == 1  && n.Right.Left.value == 1{
		zeroNodeLeft.value = 1
	} else if n.Left.Left.value == 0 && n.Left.Right.value == 1  && n.Right.Left.value == 0{
		zeroNodeLeft.value = 1
	} else if n.Left.Left.value == 0 && n.Left.Right.value == 0  && n.Right.Left.value == 1{
		zeroNodeLeft.value = 1
	} else{
		zeroNodeLeft.value = 0
	}	
	//for result.right:
	if n.Left.Right.value == 1 && n.Right.Left.value ==0 && n.Right.Right.value == 0 {
		zeroNodeRight.value = 1
	} else if n.Left.Right.value == 0 && n.Right.Left.value == 1  && n.Right.Right.value == 1{
		zeroNodeRight.value = 1
	} else if n.Left.Right.value == 0 && n.Right.Left.value == 1  && n.Right.Right.value == 0{
		zeroNodeRight.value = 1
	} else if n.Left.Right.value == 0 && n.Right.Left.value == 0  && n.Right.Right.value == 1{
		zeroNodeRight.value = 1
	} else{
		zeroNodeRight.value = 0
	}	


	// zeroNodeLeft.value = int8(rand.Int() % 2)
	// zeroNodeRight.value = int8(rand.Int() % 2)

	zeroNodeLeft.initSignature()
	zeroNodeRight.initSignature()

	// create level 1 node
	result := &Node{
		level: 1,
		Left:  zeroNodeLeft,
		Right: zeroNodeRight,
	}
	result.initSignature()

	//if table.Contain(result.Left) {
	//	result.Left = table.GetNode(result.Left.sign)
	//}
	//if table.Contain(result.Right) {
	//	result.Right = table.GetNode(result.Right.sign)
	//}

	if table.Contain(result) {
		finalResult := table.GetNode(result.sign)
		n.Result = finalResult
	} else {
		table.SaveNode(result)
		n.Result = result
	}
	//panic("classicCalcl lvl1 node not in list!")
}

func (n *Node) initSignature() {
	if n.level == 0 {
		tmp := make([]byte, 2)
		binary.BigEndian.PutUint16(tmp, uint16(n.value))
		n.sign = sha256.Sum256(tmp)
		return
	}

	lvl := make([]byte, 2)
	binary.BigEndian.PutUint16(lvl, n.level)
	combineValue := append(append(lvl, n.Left.sign[:]...), n.Right.sign[:]...)
	n.sign = sha256.Sum256(combineValue)
}

func Rebuild(n *Node, t *Table) *Node {
	u := Node{
		level: n.level+1,
		Left:  &Node{level: n.level},
		Right: &Node{level: n.level},
	}
	u.Left.Left = n.Right
	u.Left.Right = n.Left
	u.Left.initSignature()
	u.Right.Left = n.Right
	u.Right.Right = n.Left
	u.Right.initSignature()
	u.initSignature() // useless

	if t.Contain(u.Left) {
		u.Left = t.GetNode(u.Left.sign)
	}
	if t.Contain(u.Right) {
		u.Right = t.GetNode(u.Right.sign)
	}

	return &u
}