package algo

import (
	"math/rand"
	"time"
	"log"
)
// import "fmt"

func Initialize (lvl uint16) (*Node, *Table) {
	defer TimeTaken(time.Now(), "Initialze")
	n := &Node{level: lvl, value:-1}
	t := &Table{list: make(map[string]*Node)}
	n.buildSubTree(t)
	t.SaveNode(n)
	return n, t
}

func TimeTaken(t time.Time, name string) {
    elapsed := time.Since(t)
    log.Printf("TIME: %s took %s\n", name, elapsed)
}


func (n *Node) buildSubTree(table *Table) {
	// level 0
	if n.level == 0 {
		n.value = int8(rand.Int() % 2)
		//n.value = int8(0)
		n.initSignature()
		return
	}

	// build 2 sub trees
	left := &Node{level:n.level-1, value:-1}
	right := &Node{level:n.level-1, value:-1}
	left.buildSubTree(table)
	right.buildSubTree(table)

	// Left
	if table.Contain(left) {
		finalLeft := table.GetNode(left.sign)
		n.Left = finalLeft
	} else {
		table.SaveNode(left)
		n.Left = left
	}

	// Right
	if table.Contain(right) {
		finalRight := table.GetNode(right.sign)
		n.Right = finalRight
	} else {
		table.SaveNode(right)
		n.Right = right
	}

	n.initSignature()
}


func  ExtendTree(n *Node, t *Table) (*Node, *Table) {

	nlevel := n.GetLevel()

	nullnode := &Node{level: nlevel-1, value:-1}
	nullnode.buildSubTree(t)

	// nullnode.Display()
	// fmt.Println("")

	newLeft := &Node{level: nlevel, value:-1, Left: nullnode, Right : n.Left}
	newRight := &Node{level: nlevel, value:-1, Left: n.Right, Right : nullnode}
	
	newLeft.initSignature()
	newRight.initSignature()

	newNode := &Node{level: nlevel+1, value: -1, Left: newLeft, Right: newRight }
	newNode.initSignature()

	t.SaveNode(newLeft)
	t.SaveNode(newRight)
	t.SaveNode(newNode)
	return newNode, t
}

func (n *Node) buildnullTree(table *Table) {
	// level 0
	if n.level == 0 {
		n.value = int8(0)
		//n.value = int8(0)
		n.initSignature()
		return
	}

	// build 2 sub trees
	left := &Node{level:n.level-1, value:-1}
	right := &Node{level:n.level-1, value:-1}
	left.buildnullTree(table)
	right.buildnullTree(table)

	// Left
	if table.Contain(left) {
		finalLeft := table.GetNode(left.sign)
		n.Left = finalLeft
	} else {
		table.SaveNode(left)
		n.Left = left
	}

	// Right
	if table.Contain(right) {
		finalRight := table.GetNode(right.sign)
		n.Right = finalRight
	} else {
		table.SaveNode(right)
		n.Right = right
	}

	n.initSignature()
}